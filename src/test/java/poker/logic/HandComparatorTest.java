package poker.logic;

import poker.logic.bot.SimpleBot;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HandComparatorTest {

    private HandComparator hc;
    private Player p1;
    private Player p2;
    List<Player> players;
    List<Card> board;

    @Before
    public void setUp() {
        hc = new HandComparator();
        p1 = new SimpleBot("p1", 100.0);
        p2 = new SimpleBot("p2", 100.0);
        players = new ArrayList<>();
        players.addAll(Arrays.asList(p1, p2));
        board = new ArrayList<>();
    }

    @After
    public void tearDown() {
        hc = null;
        p1.clearCards();
        p2.clearCards();
        board.clear();
    }

    @Test
    public void StraightFlushBeatsFourOfKind() {
        board.addAll(Arrays.asList(new Card(0, 3), new Card(1, 3), new Card(3, 6), new Card(3, 7), new Card(3, 9)));
        p1.addCard(new Card(3, 3));
        p1.addCard(new Card(3, 3)); //33
        p2.addCard(new Card(3, 8));
        p2.addCard(new Card(3, 10)); //8Ts
        hc.compareWithBoard(players, board);
        assertEquals(1, hc.getWinners().size());
        assertEquals(p2, hc.getSortedHands().get(0).getPlayer());
    }

    @Test
    public void fourOfKindBeatsFullhouse() {
        board.addAll(Arrays.asList(new Card(0, 3), new Card(1, 3), new Card(0, 6), new Card(3, 8), new Card(0, 13)));
        p1.addCard(new Card(2, 3));
        p1.addCard(new Card(3, 3)); //33
        p2.addCard(new Card(1, 8));
        p2.addCard(new Card(2, 8)); //88
        hc.compareWithBoard(players, board);
        assertEquals(1, hc.getWinners().size());
        assertEquals(p1, hc.getSortedHands().get(0).getPlayer());
    }

    @Test
    public void fullhouseBeatsFlush() {
        board.addAll(Arrays.asList(new Card(0, 3), new Card(1, 3), new Card(0, 6), new Card(3, 8), new Card(0, 13)));
        p1.addCard(new Card(0, 10));
        p1.addCard(new Card(0, 11)); //TJs
        p2.addCard(new Card(1, 8));
        p2.addCard(new Card(2, 8)); //88
        hc.compareWithBoard(players, board);
        assertEquals(1, hc.getWinners().size());
        assertEquals(p2, hc.getSortedHands().get(0).getPlayer());
    }

    @Test
    public void flushBeatsStraight() {
        board.addAll(Arrays.asList(new Card(0, 2), new Card(1, 3), new Card(0, 6), new Card(3, 8), new Card(0, 13)));
        p1.addCard(new Card(1, 2));
        p1.addCard(new Card(2, 5)); //45o
        p2.addCard(new Card(0, 1));
        p2.addCard(new Card(0, 12)); //AQs
        hc.compareWithBoard(players, board);
        assertEquals(1, hc.getWinners().size());
        assertEquals(p2, hc.getSortedHands().get(0).getPlayer());
    }

    @Test
    public void aceHighStraightBeatsQueenHighStraight() {
        board.addAll(Arrays.asList(new Card(0, 2), new Card(1, 3), new Card(0, 10), new Card(3, 11), new Card(0, 12)));
        p2.addCard(new Card(1, 1));
        p2.addCard(new Card(2, 13)); //AKo
        p1.addCard(new Card(0, 8));
        p1.addCard(new Card(1, 9)); //89o
        hc.compareWithBoard(players, board);
        assertEquals(1, hc.getWinners().size());
        assertEquals(p2, hc.getSortedHands().get(0).getPlayer());
    }

    @Test
    public void straightBeatsTreeOfKind() {
        board.addAll(Arrays.asList(new Card(1, 2), new Card(2, 3), new Card(1, 6), new Card(3, 8), new Card(1, 13)));
        p1.addCard(new Card(2, 4));
        p1.addCard(new Card(3, 5)); //45o
        p2.addCard(new Card(2, 13));
        p2.addCard(new Card(3, 13)); //Makes set of Kings
        hc.compareWithBoard(players, board);
        assertEquals(1, hc.getWinners().size());
        assertEquals(p1, hc.getSortedHands().get(0).getPlayer());
    }

    @Test
    public void straightBeatsTwopair() {
        board.addAll(Arrays.asList(new Card(0, 13), new Card(0, 12), new Card(1, 12), new Card(1, 10), new Card(3, 3)));
        p1.addCard(new Card(1, 1));
        p1.addCard(new Card(3, 11)); //AJo makes straight
        p2.addCard(new Card(3, 2));
        p2.addCard(new Card(2, 2)); //22+QQ = TWOPAIRS
        hc.compareWithBoard(players, board);
        assertEquals(1, hc.getWinners().size());
        assertEquals(hc.getWinners().get(0), hc.getSortedHands().get(0));
        assertEquals(p1, hc.getSortedHands().get(0).getPlayer());
    }

    @Test
    public void threeOfKindSplits() { // Both have 888AK
        board.addAll(Arrays.asList(new Card(3, 13), new Card(1, 8), new Card(3, 5), new Card(2, 1), new Card(2, 8)));
        p1.addCard(new Card(1, 12));
        p1.addCard(new Card(3, 8));
        p2.addCard(new Card(0, 8));
        p2.addCard(new Card(3, 4)); 
        hc.compareWithBoard(players, board);
        assertEquals(2, hc.getWinners().size());
    }

    @Test
    public void threeOfKindBeatsTwoPairs() {
        board.addAll(Arrays.asList(new Card(1, 2), new Card(2, 3), new Card(1, 6), new Card(3, 8), new Card(1, 13)));
        p1.addCard(new Card(2, 2));
        p1.addCard(new Card(3, 2)); //pair of deuces makes set
        p2.addCard(new Card(2, 8));
        p2.addCard(new Card(3, 13)); //8K makes twopairs
        hc.compareWithBoard(players, board);
        assertEquals(1, hc.getWinners().size());
        assertEquals(p1, hc.getSortedHands().get(0).getPlayer());
    }

    @Test
    public void threeOfKindBeatsQueens() {
        board.addAll(Arrays.asList(new Card(1, 2), new Card(2, 3), new Card(1, 6), new Card(3, 8), new Card(1, 13)));
        p1.addCard(new Card(2, 2));
        p1.addCard(new Card(3, 2)); //KAKKOSPARI ELI SETTI
        p2.addCard(new Card(1, 12));
        p2.addCard(new Card(3, 12)); //AKKAPARI
        hc.compareWithBoard(players, board);
        assertEquals(1, hc.getWinners().size());
        assertEquals(p1, hc.getSortedHands().get(0).getPlayer());
    }

    @Test
    public void BetterTwoPairWins() { //TT77A vs AA77J
        board.addAll(Arrays.asList(new Card(1, 7), new Card(3, 7), new Card(2, 10), new Card(0, 1), new Card(1, 11)));
        p1.addCard(new Card(0, 10));
        p1.addCard(new Card(3, 8)); //TT77A
        p2.addCard(new Card(1, 1));
        p2.addCard(new Card(0, 13)); //AA77J
        hc.compareWithBoard(players, board);
        assertEquals(1, hc.getWinners().size());
        assertEquals(p2, hc.getWinners().get(0).getPlayer());
    }

    @Test
    public void BetterTwoPairWins2() { //77TTA vs 44TTA
        board.addAll(Arrays.asList(new Card(0, 10), new Card(2, 7), new Card(1, 10), new Card(2, 1), new Card(1, 4)));
        p2.addCard(new Card(2, 6));
        p2.addCard(new Card(3, 7)); //77TTA
        p1.addCard(new Card(3, 4));
        p1.addCard(new Card(0, 6)); //44TTA
        hc.compareWithBoard(players, board);
        assertEquals(1, hc.getWinners().size());
        assertEquals(hc.getWinners().get(0).getPlayer(), p2);
    }

    @Test
    public void BetterTwoPairWins3() { //66KKA vs JJKK9
        board.addAll(Arrays.asList(new Card(0, 6), new Card(2, 13), new Card(1, 11), new Card(2, 13), new Card(1, 6)));
        p1.addCard(new Card(3, 1));
        p1.addCard(new Card(0, 12)); //66KKA
        p2.addCard(new Card(2, 11));
        p2.addCard(new Card(3, 9)); //JJKK9
        hc.compareWithBoard(players, board);
        assertEquals(1, hc.getWinners().size());
        assertEquals(hc.getWinners().get(0).getPlayer(), p2);
    }

    @Test
    public void twoPairsBeatsQueens() {
        board.addAll(Arrays.asList(new Card(1, 2), new Card(2, 3), new Card(1, 6), new Card(3, 8), new Card(1, 13)));
        p2.addCard(new Card(2, 8));
        p2.addCard(new Card(3, 13)); //8K makes twopairs
        p1.addCard(new Card(1, 12));
        p1.addCard(new Card(3, 12)); //AKKAPARI
        hc.compareWithBoard(players, board);
        assertEquals(1, hc.getWinners().size());
        assertEquals(p2, hc.getSortedHands().get(0).getPlayer());
    }

    @Test
    public void BetterPairWins() { //AA vs 77
        board.addAll(Arrays.asList(new Card(0, 4), new Card(2, 8), new Card(2, 7), new Card(2, 13), new Card(1, 1)));
        p1.addCard(new Card(2, 5));
        p1.addCard(new Card(0, 1)); //AA
        p2.addCard(new Card(1, 7));
        p2.addCard(new Card(1, 9)); //77
        hc.compareWithBoard(players, board);
        assertEquals(1, hc.getWinners().size());
        assertEquals(hc.getWinners().get(0).getPlayer(), p1);
    }

    @Test
    public void AcessBeatsHighCard() {
        board.addAll(Arrays.asList(new Card(2, 2), new Card(3, 13), new Card(2, 1), new Card(3, 10), new Card(3, 7)));
        p1.addCard(new Card(0, 5));
        p1.addCard(new Card(1, 4));
        p2.addCard(new Card(3, 1));
        p2.addCard(new Card(2, 9));
        hc.compareWithBoard(players, board);
        assertEquals(1, hc.getWinners().size());
        assertEquals(p2, hc.getSortedHands().get(0).getPlayer());
    }

    @Test
    public void QueensBeatsHighCard() {
        board.addAll(Arrays.asList(new Card(1, 2), new Card(2, 3), new Card(1, 6), new Card(3, 8), new Card(1, 13)));
        p1.addCard(new Card(2, 10));
        p1.addCard(new Card(3, 11)); //TJo
        p2.addCard(new Card(1, 12));
        p2.addCard(new Card(3, 12)); //Pair of queens
        hc.compareWithBoard(players, board);
        assertEquals(1, hc.getWinners().size());
        assertEquals(p2, hc.getSortedHands().get(0).getPlayer());
    }

    @Test
    public void PairWinsHighCard() {
        board.addAll(Arrays.asList(new Card(1, 7), new Card(3, 13), new Card(1, 6), new Card(2, 4), new Card(3, 9)));
        p1.addCard(new Card(1, 9));
        p1.addCard(new Card(1, 11));
        p2.addCard(new Card(2, 6));
        p2.addCard(new Card(2, 5));
        SimpleBot p3 = new SimpleBot("p3", 100.0);
        SimpleBot p4 = new SimpleBot("p4", 100.0);
        SimpleBot p5 = new SimpleBot("p5", 100.0);
        p3.addCard(new Card(3, 8));
        p3.addCard(new Card(1, 4));
        p4.addCard(new Card(3, 5));
        p4.addCard(new Card(1, 12));
        p5.addCard(new Card(2, 10));
        p5.addCard(new Card(1, 13));
        players.addAll(Arrays.asList(p3, p4, p5));
        hc.compareWithBoard(players, board);
        assertEquals(p5, hc.getSortedHands().get(0).getPlayer());
    }

    @Test
    public void BetterHighCardWins() {
        board.addAll(Arrays.asList(new Card(1, 2), new Card(2, 3), new Card(1, 6), new Card(3, 8), new Card(1, 13)));
        p2.addCard(new Card(2, 10));
        p2.addCard(new Card(3, 11));
        p1.addCard(new Card(1, 9));
        p1.addCard(new Card(3, 10));
        hc.compareWithBoard(players, board);

        assertEquals(p2, hc.getSortedHands().get(0).getPlayer());
    }

    @Test
    public void BetterHighCardWins2() {
        board.addAll(Arrays.asList(new Card(1, 13), new Card(2, 12), new Card(1, 10), new Card(3, 13), new Card(1, 4)));
        p1.addCard(new Card(2, 3));
        p1.addCard(new Card(3, 6));
        p2.addCard(new Card(1, 2));
        p2.addCard(new Card(3, 5));
        hc.compareWithBoard(players, board);

        assertEquals(1, hc.getWinners().size());
        assertEquals(p1, hc.getWinners().get(0).getPlayer());
    }

    @Test
    public void BoardPlays() { // BOARD: ATAAA, p1: 65 p2: 55 
        board.addAll(Arrays.asList(new Card(0, 1), new Card(1, 10), new Card(1, 1), new Card(2, 1), new Card(3, 1)));
        p1.addCard(new Card(0, 6));
        p1.addCard(new Card(2, 7));
        p2.addCard(new Card(2, 5));
        p2.addCard(new Card(0, 5));
        hc.compareWithBoard(players, board);

        Card[] p1Combined = combineBoardAndHoleCards(board.toArray(new Card[board.size()]), p1.getCards().toArray(new Card[p1.getCards().size()]));
        Card[] p2Combined = combineBoardAndHoleCards(board.toArray(new Card[board.size()]), p1.getCards().toArray(new Card[p2.getCards().size()]));
        Hand p1Hand = new Hand(p1Combined, p1);
        Hand p2hand = new Hand(p2Combined, p2);
        p1Hand.determineValue();
        p2hand.determineValue();
        assertEquals(0, p1Hand.compareTo(p2hand));
        assertEquals(2, hc.getWinners().size());
    }

    private Card[] combineBoardAndHoleCards(Card[] board, Card[] hole) {
        Card[] combined = new Card[7];
        System.arraycopy(board, 0, combined, 0, board.length);
        System.arraycopy(hole, 0, combined, board.length, hole.length);
        return combined;
    }
}
