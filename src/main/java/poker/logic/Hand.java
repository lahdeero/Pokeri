package poker.logic;

import poker.logic.enums.PokerHand;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static poker.logic.enums.PokerHand.*;

public class Hand implements Comparable<Hand> {
    private Player player;
    private int[][] cardArray;
    private PokerHand pokerHand;
    private boolean determined;
    private int pairValue;
    private int secondPairValue;
    private int twoPairValue;
    private int tripsValue;
    private int quadsValue;
    private Card[] cards;

    public Hand(Card[] cards, Player player) {
        this.cards = cards;
        this.player = player;
        this.cardArray = new int[4][14];
        for (Card card : cards) {
//            System.out.println("[][]" + card.getSuit().getValue() + " " + card.getRank());
            cardArray[card.getSuit().getValue()][card.getRank()] += 1;
        }
        this.determined = false;
        this.pairValue = 0;
        this.twoPairValue = 0;
        this.tripsValue = 0;
    }

    @Override
    public String toString() {
        return this.player.getName() + " " + this.pokerHand;
    }

    public String toStringDebug() {
        String ret = "";
        for (Card card : cards) {
            ret += card.toString() + " ";
        }
        return this.player.getName() + " " + this.pokerHand + "\n" + ret;
    }

    public PokerHand determineValue() {
        determined = true;
        if (checkStraightFlush()) {
            this.pokerHand = STRAIGHTFLUSH;
            return STRAIGHTFLUSH;
        }

        PokerHand best = checkManyOfKind();
        if (best.getValue() < FULLHOUSE.getValue()) {
            if (checkFlush()) {
                best = FLUSH;
            } else if (checkStraight()) {
                best = STRAIGHT;
            }
        }
        this.pokerHand = best;
        return best;
    }

    private boolean checkStraightFlush() {
        int seq = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 1; j < 14; j++) {
                if (cardArray[i][j] > 0) {
                    seq++;
                    if (seq == 5) {
                        return true;
                    }
                } else if (j == 13 && seq == 4 && cardArray[i][1] > 1) {
                    return true;
                } else {
                    seq = 0;
                }
            }
        }
        return false;
    }

    private PokerHand checkManyOfKind() {
        List<Integer> pairs = new ArrayList<>();
        int trips = 0;
        for (int j = 1; j < 14; j++) {
            int sameKinds = 0;
            for (int i = 0; i < 4; i++) {
                if (cardArray[i][j] > 0) {
                    sameKinds += 1;
                }
                if (cardArray[i][j] > 0 && sameKinds == 2) {
                    int value = j == 1 ? 14 : j;
                    pairs.add(value);
                } else if (sameKinds == 3) {
                    if (j > tripsValue) {
                        tripsValue = j;
                    }
                    pairs.remove(Integer.valueOf(j));
                    trips += 1;
                } else if (sameKinds == 4) {
                    if (j == 1) {
                        this.quadsValue = j;
                    } else {
                        this.quadsValue = j;
                    }
                    return FOUROFKIND;
                }
            }
        }
        if (pairs.size() == 1) {
            pairValue = pairs.get(0);
        } else if (!pairs.isEmpty()) {
            Collections.sort(pairs, Collections.reverseOrder());
            twoPairValue = pairs.get(0);
            secondPairValue = pairs.get(1);
        }
        if (trips > 0 && !pairs.isEmpty()) {
            return FULLHOUSE;
        } else if (trips > 0) {
            return THREEOFKIND;
        } else if (pairs.size() >= 2) {
            return TWOPAIRS;
        } else if (pairs.size() == 1) {
            return PAIR;
        }
        return HIGHCARD;
    }

    private boolean checkFlush() {
        for (int i = 0; i < 4; i++) {
            int sameSuit = 0;
            for (int j = 1; j < 14; j++) {
                if (cardArray[i][j] > 0) {
                    sameSuit++;
                }
            }
            if (sameSuit >= 5) {
                return true;
            }
        }
        return false;
    }

    private boolean checkStraight() {
        int seq = 0;
        for (int j = 1; j < 14; j++) {
            int columnHits = 0;
            for (int i = 0; i < 4; i++) {
                if (cardArray[i][j] > 0 && columnHits == 0) {
                    columnHits += 1;
                    seq += 1;
                    if (seq == 5) {
                        return true;
                    }
                }
            }
            if (seq == 4 && j == 13 && (cardArray[0][1] > 0 || cardArray[1][1] > 0 || cardArray[2][1] > 0 || cardArray[3][1] > 0)) {
                return true;
            }
            if (columnHits == 0) {
                seq = 0;
            }
        }
        return false;
    }

    public PokerHand getPokerHand() {
        if (determined) {
            return this.pokerHand;
        }
        return determineValue();
    }

    public Player getPlayer() {
        return this.player;
    }

    @Override
    public int compareTo(Hand h) {
        if (pokerHand.getValue() == h.getPokerHand().getValue()) {
            switch (this.pokerHand) {
                case PAIR:
                    if (this.pairValue > h.getPairValue()) {
                        return 1;
                    } else if (this.pairValue < h.getPairValue()) {
                        return -1;
                    }
                    break;
                case TWOPAIRS:
                    if (this.twoPairValue > h.getTwoPairValue()) {
                        return 1;
                    } else if (this.twoPairValue < h.getTwoPairValue()) {
                        return -1;
                    } else {
                        if (this.secondPairValue - h.getSecondPairValue() != 0) {
                            return this.secondPairValue - h.getSecondPairValue();
                        }
                    }
                    break;
                case THREEOFKIND:
                    if (this.tripsValue > h.getTripsValue()) {
                        return 1;
                    } else if (this.tripsValue < h.getTripsValue()) {
                        return -1;
                    }
                    break;
                case FULLHOUSE: // Fullhouses really should be tested!
                    if (this.tripsValue > h.getTripsValue()) {
                        return 1;
                    } else if (this.tripsValue < h.getTripsValue()) {
                        return -1;
                    }
                    if (this.pairValue > h.getPairValue()) {
                        return 1;
                    } else if (this.pairValue < h.getPairValue()) {
                        return -1;
                    }
                    return 0;
                case FOUROFKIND:
                    if (this.quadsValue - h.getQuadsValue() != 0) {
                        return this.quadsValue - h.getQuadsValue();
                    }
                    break;
            }
            return betterHighCard(h.getCardArray());
        } else if (pokerHand.getValue() > h.getPokerHand().getValue()) {
            return 1;
        }
        return -1;
    }

    private int calculateRelevants() {
        switch (this.pokerHand) {
            case FOUROFKIND:
                return 4;
            case THREEOFKIND:
                return 3;
            case TWOPAIRS:
                return 4;
            case PAIR:
                return 2;
            default:
                return 0;
        }
    }

    private int betterHighCard(int[][] carray) {
        int relevantsCount = calculateRelevants();
        int p1RelevantCount = relevantsCount;
        for (int j = 14; j > 5; j--) {
            if (j == 14) {
                j = 1; // We need to start with aces
            }
            boolean p1CardFound = false;
            boolean p2CardFound = false;
            for (int i = 0; i < 4; i++) {
                if (this.cardArray[i][j] > 0) {
                    if (p1CardFound) { // Since we dont care about pairs
                        p1RelevantCount -= 1;
                        continue;
                    }
                    p1RelevantCount += 1;
                    p1CardFound = true;
                }
                if (carray[i][j] > 0) {
                    p2CardFound = true;
                }
                if (p1CardFound && p2CardFound && p1RelevantCount >= 5) {
                    return 0;
                }
            }

            if (p1CardFound && !p2CardFound) {
                return 1;
            } else if (p2CardFound && !p1CardFound) {
                return -1;
            }

            if (j == 1) {
                j = 14; 
            }
        }
        return 0;
    }

    public int[][] getCardArray() {
        return this.cardArray;
    }

    public int getPairValue() {
        return this.pairValue;
    }

    public int getTwoPairValue() {
        return this.twoPairValue;
    }

    public int getSecondPairValue() {
        return this.secondPairValue;
    }

    public int getTripsValue() {
        return this.tripsValue;
    }

    public int getQuadsValue() {
        return this.quadsValue;
    }
}
