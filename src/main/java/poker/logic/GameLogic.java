package poker.logic;

import java.util.List;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Stack;
import java.util.logging.Level;
import poker.logic.Deck;
import poker.logic.Action;
import poker.logic.enums.HandState;
import poker.ui.FxGui;
import static poker.logic.enums.HandState.*;
import poker.logic.enums.PokerAction;
import static poker.logic.enums.PokerAction.*;
import poker.ui.UserInterface;

public class GameLogic {

    private final static Logger LOGGER = Logger.getLogger(GameLogic.class.getName());
    HandComparator hc;
    private List<Player> players; // HUMAN[0] and AI[1]
    private UserInterface fxGui;
    private final String cardImagePath;
    private List<Card> board;
    private Deck deck;

    private int handNumber;
    private int dealerButton;
    private int playerTurn;
    private double pot;
    private HandState handState;
    private int playersInHand;
    private double potLog[][];
    private int turns;
    private Player seats[];
    private Player ai;
    private Stack<Action> handActions;

    public final double bigBlind = 2;
    public final double smallBlind = bigBlind * 0.5;

    public GameLogic(List<Player> players, UserInterface fxGui, String cardImagePath) {
        LOGGER.setLevel(Level.INFO);
        hc = new HandComparator();
        this.players = players;
        this.fxGui = fxGui;
        this.cardImagePath = cardImagePath;
        this.board = new ArrayList<>();
        this.seats = new Player[10];
        int i = 0;
        for (Player player : players) {
            this.seats[i++] = player;
        }
        this.ai = players.get(1);
        this.handNumber = 0;
        this.playerTurn = 0;
    }

    public void init() {
        this.handState = NEWHAND;
        this.dealerButton = 0;
        this.deck = new Deck(cardImagePath);
        this.handActions = new Stack();
    }

    public void startNewHand() {
        this.handActions.clear();
        board.clear();
        deck.refill();
        deck.shuffle();
        resetPotAndLog();
        this.dealerButton = dealerButton + 1 == players.size() ? 0 : dealerButton + 1;
        this.playersInHand = players.size();
        this.turns = players.size();
        postBlinds();
        dealHoleCards();
        this.playerTurn = dealerButton;
        for (Player player : players) {
            player.prepareForNewHand();
        }
        if (seats[playerTurn].getType().equals("bot")) {
            evokeAi(amountForCall(true));
        } else {
            this.fxGui.setPlayable(true);
        }
    }

    /** NOT SURE IF THIS METHOD IS GOOD IDEA AT ALL.. */
    private void postBlinds() {
        this.handState = PREFLOP;
        int sb = dealerButton;
        int bb = dealerButton + 1 == playersInHand ? 0 : dealerButton + 1;
        seats[sb].post(this.smallBlind);
        seats[bb].post(this.bigBlind);
        seats[sb].reduceChips(this.smallBlind);
        seats[bb].reduceChips(this.bigBlind);
        
        handActions.push(new Action(seats[sb], POST, smallBlind, this.handState));
        updatePotAndLog(handActions.peek());
        this.fxGui.updateUi(handActions.peek(), pot, 0);
        
        handActions.push(new Action(seats[bb], POST, bigBlind, this.handState));
        updatePotAndLog(handActions.peek());
        this.fxGui.updateUi(handActions.peek(), pot, smallBlind);
    }

    public double amountForCall(boolean playersMove) {
        printPotLog();
        System.out.println("HANDSTATE NUMBER = " + handState.asNumber());
        double max = 0;
        int currentStreet = handState.asNumber();
        for (int i = 0; i < players.size(); i++) {
            if (potLog[i][currentStreet] > max) {
                max = potLog[i][currentStreet];
            }
        }
        if (!playersMove) {
            int p = playerTurn == 0 ? 1 : 0;
            return max - potLog[p][currentStreet];
        }
        return max - potLog[playerTurn][currentStreet];
    }

    private void dealHoleCards() {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < players.size(); j++) {
                Card card = deck.drawCard();
                players.get(i).addCard(card);
            }
        }
        for (Player player : players) {
            for (Card card : player.getCards()) {
                this.fxGui.dealCard(player, card);
            }
        }
    }

    public void pushHumanAction(Action action) {
        fxGui.setPlayable(false);
        if (!seats[playerTurn].getType().equals("human")) {
            LOGGER.warning("NOT YOUR TURN");
            return;
        }
        int endHand = updateLogic(action);
        if (endHand == 1) {
            showDown();
        } else {
            if (seats[playerTurn].getType().equals("human")) {
                fxGui.setPlayable(true);
                return;
            }
            evokeAi(amountForCall(true));
        }
    }
    
    public double amountForMinRaise() {
        if (handActions.isEmpty() || handActions.peek().getPokerAction() == PokerAction.POST) {
            return bigBlind * 2;
        }
        if (handActions.peek().getPokerAction() == BET
                || handActions.peek().getPokerAction() == RAISE) {
            return handActions.peek().getAmount() * 2;
        }
        return 2;
    }

    public void evokeAi(double currentRaise) {
        System.out.println("current raise = " + currentRaise);
        ai.prepareForAction(board, currentRaise, amountForMinRaise(), this.handState);
        Action aiAction = ai.getAction();
        System.out.println("AiAction.amount = " + aiAction.getAmount());
        pushAiAction(aiAction);
    }

    private void pushAiAction(Action action) {
        int endHand = updateLogic(action);
        if (endHand == 1) {
            showDown();
        } else {
            boolean doubleTurn = seats[playerTurn].getType().equals("bot");
            /* HAPPENS IN HEADS-UP TEXAS AFTER PREFLOP */
            if (doubleTurn) {
                fxGui.setPlayable(false);
                evokeAi(0);
            } else {
                fxGui.setPlayable(true);
            }
        }
    }

    /**
     * logic, no difference between human and bot. return: 2 = END HAND
     */
    private int updateLogic(Action action) {
        this.handActions.add(action);
        if (this.handState.asNumber() > RIVER.asNumber()) {
            LOGGER.severe("ERROR: SHOULD NOT UPDATE AFTER RIVER");
            return 2;
        }
        double chipsToPot = action.getPlayer().reduceChips(action.getAmount());
        if (action.getPlayer().getChips() == 0) {
            action.setPokerAction(PokerAction.ALLIN);
            action.setAmount(chipsToPot);
        }
        updatePotAndLog(action);

        switch (action.getPokerAction()) {
            case FOLD:
                this.playersInHand -= 1;
                this.turns -= 1;
                break;
            case CHECK:
                this.turns -= 1;
                break;
            case CALL:
                this.turns -= 1;
                break;
            case BET:
                this.turns = playersInHand - 1;
            case RAISE:
                this.turns = playersInHand - 1;
            default:
                break;
        }

        int x = figureLogicsNextMove(action);
        System.out.println("amountForCall = " + amountForCall(true));
        this.fxGui.updateUi(action, pot, amountForCall(true));
        return x;
    }

    private int figureLogicsNextMove(Action action) {
        if (allInMoment(action)) {
            while (this.handState.asNumber() < HandState.RIVER.asNumber()) {
                this.handState = this.handState.next();
                dealNextStreet();
            }
            return 1;
        } else if (playersInHand > 1 && turns == 0) {
            if (handState == RIVER) {
                return 1;
            }
            turns = playersInHand;
            this.handState = this.handState.next();
            dealNextStreet();
            this.playerTurn = this.dealerButton + 1 == playersInHand ? 0 : this.dealerButton + 1;
        } else if (playersInHand > 1) {
            this.playerTurn = this.playerTurn + 1 == playersInHand ? 0 : this.playerTurn + 1;
        } else if (playersInHand == 1) {
            this.playerTurn = this.playerTurn + 1 >= players.size() ? 0 : this.playerTurn + 1;
            return 1;
        } else {
            return 1;
        }
        return 0;
    }

    public boolean isCurrentStreetBetted() {
        if (handActions.isEmpty()) {
            System.out.println("empty");
            return true;
        }
        Action lastAction = handActions.peek();
        if (lastAction.getPokerAction() == PokerAction.POST
                || lastAction.getPokerAction() == PokerAction.BET
                || lastAction.getPokerAction() == PokerAction.RAISE
                || lastAction.getPokerAction() == PokerAction.ALLIN) {
            System.out.println("betted");
            return true;
        }
        System.out.println("no bet");
        return false;
    }

    private boolean allInMoment(Action action) {
        if ((action.getPokerAction() == CALL && countAllIns() >= players.size() - 1) || action.getPokerAction() == PokerAction.ALLIN && countAllIns() == players.size()) {
            return true;
        } else if (action.getPokerAction() == PokerAction.ALLIN) {
            Action current = this.handActions.pop();
            Action previous = this.handActions.peek();
            if ((previous.getPokerAction() == BET || previous.getPokerAction() == RAISE) && previous.getAmount() > action.getAmount()) {
                handActions.add(current);
                return true;
            }
        }
        return false;
    }

    private int countAllIns() {
        int count = 0;
        for (Player player : players) {
            if (player.getChips() == 0) {
                count += 1;
            }
        }
        return count;
    }

    private void resetPotAndLog() {
        fxGui.resetPot();
        this.potLog = new double[players.size()][8];
        this.pot = 0;
    }

    private void updatePotAndLog(Action action) {
        int currentStreet = this.handState.asNumber();
        this.potLog[findPlayersSeat(action.getPlayer())][currentStreet] += action.getAmount();
        double sum = 0;
        for (int y = 0; y < players.size(); y++) {
            for (int x = 0; x < 8; x++) {
                sum += potLog[y][x];
            }
        }
        this.pot = sum;
    }

    private int findPlayersSeat(Player player) {
        for (int i = 0; i < players.size(); i++) {
            if (players.get(i).equals(player)) {
                return i;
            }
        }
        return -1;
    }

    private void dealNextStreet() {
        Card card = null;
        switch (handState) {
            case FLOP:
                for (int i = 0; i < 3; i++) {
                    card = deck.drawCard();
                    board.add(card);
                    fxGui.dealBoard(card);
                }
                break;
            case TURN:
                card = deck.drawCard();
                board.add(card);
                fxGui.dealBoard(card);
                break;
            case RIVER:
                card = deck.drawCard();
                board.add(card);
                fxGui.dealBoard(card);
                break;
            case SHOWDOWN:
                break;
            default:
                LOGGER.severe("ERROR: HANDSTATE DEFAULT");
                break;
        }
    }

    /**
     * It's really showdown only if there are multiple players left in hand
     */
    private void showDown() {
        fxGui.setPlayable(false);
        fxGui.chatAppend("END OF HAND!");

        if (playersInHand > 1) {
            fxGui.showDown();
            hc.compareWithBoard(players, board);
            List<Hand> winners = hc.getWinners();
            if (winners.size() > 1) {
                if (winners.get(0).compareTo(winners.get(1)) != 0) {
                    LOGGER.severe("WINNERS TWO HANDS BUT NOT SAME COMPARE");
                }
                fxGui.chatAppend("");
                fxGui.chatAppend("SPLIT: ");
                fxGui.chatAppend(winners.get(0).toString() + " " + this.pot / 2);
                fxGui.chatAppend(winners.get(1).toString() + " " + this.pot / 2);
                winners.get(0).getPlayer().addChips(pot / 2);
                winners.get(1).getPlayer().addChips(pot / 2);
            } else if (winners.size() == 1) {
                fxGui.chatAppend("");
                fxGui.chatAppend("WINNER: ");
                fxGui.chatAppend(winners.get(0).toString() + " " + this.pot);
                winners.get(0).getPlayer().addChips(pot);
            } else {
                LOGGER.severe("JUST TWO PLAYERS SUPPORTED IN SHOWDOWN");
            }

            fxGui.chatAppend("");
            fxGui.chatAppend("End of showdonw");
        } else {
            fxGui.chatAppend("WINNER: ");
            fxGui.chatAppend(seats[playerTurn].getName() + " " + this.pot);
            fxGui.chatAppend("NOSHOW");
            seats[playerTurn].addChips(pot);
        }
        endHand();
    }

    private void endHand() {
//        printPotLog();
        for (Player player : players) {
            player.clearCards();
        }
        this.board.clear();
        fxGui.setPlayable(!checkAllIn());
        fxGui.updateUiAfterHand(dealerButton);
        fxGui.endHand();
    }

    private boolean checkAllIn() {
        for (Player player : players) {
            if (player.getChips() < bigBlind) {
                return true;
            }
        }
        return false;
    }

    public int getHandNumber() {
        return handNumber;
    }

    public HandState getState() {
        return handState;
    }

    public Player getPlayerOne() {
        return players.get(0);
    }

    public HandState getStreet() {
        return handState;
    }

    private void printPotLog() {
        for (int y = 0; y < players.size(); y++) {
            for (int x = 0; x < 8; x++) {
                System.out.print(potLog[y][x] + " ");
            }
            System.out.println();
        }
    }

    public HandState getHandState() {
        return this.handState;
    }

    public double getSmallBlindSize() {
        return this.smallBlind;
    }

    public double getBigBlindSize() {
        return this.bigBlind;
    }
}
