package poker.logic.bot;

import poker.logic.Action;
import poker.logic.Card;
import poker.logic.Player;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import poker.logic.enums.HandState;
import static poker.logic.enums.PokerAction.*;

public class SimpleBot implements Player {
    private String name;
    private List<Card> cards;
    private double chips;
    private Action action;
    private boolean sitout;
    private boolean present;
    private boolean allin;
    private String type = "bot";
    private double currentlyChipsIn;
    private HandState handState;

    public SimpleBot(String name, double chips) {
        this.name = name;
        this.cards = new ArrayList<>();
        this.chips = chips;
        this.present = true;
        this.currentlyChipsIn = 0.0;
    }

    public void prepareForNewHand() {
        this.currentlyChipsIn = 0.0;
    }

    public void prepareForAction(List<Card> board, double currentBet, double minRaise, HandState handState) {
        this.handState = handState;
        if (currentBet == 0) {
            this.action = new Action(this, CHECK, 0, this.handState);
        } else if (currentBet <= chips) {
            this.action = new Action(this, CALL, currentBet, this.handState);
        } else {
            this.action = new Action(this, ALLIN, this.chips, this.handState);
        }
    }

    public Action getAction() {
        this.currentlyChipsIn += action.getAmount();
        return this.action;
    }

    public double post(double amount) {
        if (chips - amount >= 0) {
            currentlyChipsIn += amount;
            return amount;
        }
        return 0;
    }

    public void addChips(double amount) {
        if (amount > 0.0) {
            chips += amount;
        }
    }

    @Override
    public double reduceChips(double amount) {
        if (chips >= amount) {
            chips -= amount;
            return amount;
        } 
        double allChips = chips;
        chips = 0;
        return allChips;
    }

    public double getChips() {
        return this.chips;
    }

    public String getName() {
        return this.name;
    }

    public void addCard(Card card) {
        this.cards.add(card);
    }

    public List<Card> getCards() {
        return this.cards;
    }

    @Override
    public void clearCards() {
        this.cards.clear();
    }

    public void setAllin(boolean allin) {
        this.allin = allin;
    }

    public boolean getAllin() {
        return this.allin;
    }

    public void setPresent(boolean present) {
        this.present = present;
    }

    public boolean getPresent() {
        return this.present;
    }

    public void setSitout(boolean sitout) {
        this.sitout = sitout;
    }

    public boolean getSitout() {
        return this.sitout;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SimpleBot)) {
            return false;
        }
        SimpleBot player = (SimpleBot) o;
        return type == player.type
                && name.equals(player.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type);
    }

    @Override
    public String getType() {
        return this.type;
    }
}
