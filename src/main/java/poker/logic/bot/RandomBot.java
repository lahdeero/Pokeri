package poker.logic.bot;

import java.util.ArrayList;
import java.util.List;
import poker.logic.enums.HandState;
import poker.logic.Action;
import poker.logic.Card;
import poker.logic.Player;
import poker.logic.enums.PokerAction;

public class RandomBot implements Player {

    private String name;
    private List<Card> cards;
    private double chips;
    private Action action;
    private boolean sitout;
    private boolean present;
    private boolean allin;
    private String type = "bot";
    private HandState handState;

    private boolean[][] value = new boolean[13][13];

    public RandomBot(String name, double chips) {
        this.name = name;
        this.cards = new ArrayList<>();
        this.chips = chips;
        this.present = true;
    }

    @Override
    public void prepareForNewHand() {
    }

    @Override
    public void prepareForAction(List<Card> board, double currentBet, double minRaise, HandState handState) {
        int r = (int) (Math.random() * 7 + 1);
        System.out.println("r = " + r);
        switch (r) {
            case (1):
                if (currentBet == 0) {
                    this.action = new Action(this, PokerAction.CHECK, 0, handState);
                } else {
                    this.action = new Action(this, PokerAction.FOLD, 0, handState);
                }
                break;
            case (2):
            case (3):
            case (4):
                if (currentBet == 0) {
                    this.action = new Action(this, PokerAction.BET, 2, handState);
                } else {
                    this.action = new Action(this, PokerAction.CALL, currentBet, handState);
                }
                break;
            default:
                if (currentBet == 0) {
                    this.action = new Action(this, PokerAction.BET, 6, handState);
                } else if (this.chips > minRaise) {
                    this.action = new Action(this, PokerAction.RAISE, minRaise, handState);
                } else {
                    this.action = new Action(this, PokerAction.ALLIN, this.chips, handState);
                }
        }
    }

    public Action getAction() {
        System.out.println("action.amount in class = " + action.getAmount());
        return this.action;
    }

    private void testSouts() {
        System.out.println("---RANDOMBOT ACTION--------");
        System.out.println(cards.get(0) + " " + cards.get(1));
        System.out.println(action);
        System.out.println("------------------------");
    }

    public double post(double amount) {
        if (chips - amount >= 0) {
            return amount;
        }
        System.out.println("BLIND POST ERROR");
        return 0;
    }

    public void addChips(double amount) {
        if (amount > 0.0) {
            chips += amount;
        }
    }

    @Override
    public double reduceChips(double amount) {
        if (chips >= amount) {
            chips -= amount;
            System.out.println("chips = " + this.chips);
            return amount;
        }
        double allChips = chips;
        chips = 0;
        return allChips;
    }

    public double getChips() {
        return this.chips;
    }

    public String getName() {
        return this.name;
    }

    public void addCard(Card card) {
        this.cards.add(card);
    }

    public List<Card> getCards() {
        return this.cards;
    }

    public void clearCards() {
        this.cards.clear();
    }

    public void setAllin(boolean allin) {
        this.allin = allin;
    }

    public boolean getAllin() {
        return this.allin;
    }

    public void setPresent(boolean present) {
        this.present = present;
    }

    public boolean getPresent() {
        return this.present;
    }

    public void setSitout(boolean sitout) {
        this.sitout = sitout;
    }

    public boolean getSitout() {
        return this.sitout;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RandomBot other = (RandomBot) obj;
        if (this.type != other.type) {
            return false;
        }

        return true;
    }

    @Override
    public String getType() {
        return this.type;
    }
}
