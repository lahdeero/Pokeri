package poker.logic.bot;

import poker.logic.Action;
import poker.logic.Card;
import poker.logic.Player;

import java.util.ArrayList;
import java.util.List;
import poker.logic.enums.HandState;

public class Human implements Player {

    private String name;
    private List<Card> cards;
    private double chips;
    private Action action;
    private boolean sitout;
    private boolean present;
    private boolean allin;
    private String type = "human";

    public Human(String name, double chips) {
        System.out.println("PLAYER NAME: " + name);
        this.name = name;
        this.chips = chips;
        this.cards = new ArrayList<>();
    }

    @Override
    public void prepareForNewHand() {
    }

    @Override
    public void prepareForAction(List<Card> board, double currentBet, double minRaise, HandState handState) {
    }

    public void setAction(Action action) {
        this.action = action;
    }

    @Override
    public Action getAction() {
        return this.action;
    }

    @Override
    public double post(double amount) {
        return amount;
    }

    @Override
    public void addChips(double amount) {
        chips += amount;
    }

    @Override
    public double reduceChips(double amount) {
        if (chips >= amount) {
            chips -= amount;
            return amount;
        } 
        double allChips = chips;
        chips = 0;
        return allChips;
    }

    @Override
    public double getChips() {
        
        return this.chips;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void addCard(Card card) {
        this.cards.add(card);
    }

    @Override
    public List<Card> getCards() {
        return this.cards;
    }

    @Override
    public void clearCards() {
        this.cards.clear();
    }

    @Override
    public void setAllin(boolean allin) {
        this.allin = true;
    }

    @Override
    public boolean getAllin() {
        return this.allin;
    }

    @Override
    public void setPresent(boolean present) {
        this.present = present;
    }

    @Override
    public boolean getPresent() {
        return this.present;
    }

    @Override
    public void setSitout(boolean sitout) {
        this.sitout = sitout;
    }

    public boolean equals(Player p) {
        return this.name.equals(p.getName());
    }

    @Override
    public boolean getSitout() {
        return this.sitout;
    }

    @Override
    public String getType() {
        return this.type;
    }
}
