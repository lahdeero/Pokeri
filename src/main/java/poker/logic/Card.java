package poker.logic;

import poker.logic.enums.Suit;
import javafx.scene.Parent;
import javafx.scene.image.ImageView;
import java.util.Objects;
import poker.ui.ImageAdder.ImageAdder;

public class Card extends Parent {

    private final Suit suit;
    private final int rank;
    public ImageView imgView;
    private String cardImagePath = "/cards/png/";
    private String cardBackPath = cardImagePath + "BackBlue.png";
    private boolean hidden;
    public Card(int suit, int rank) {
        this.hidden = hidden;
        Suit suittemp = null;
        switch (suit) {
            case 0:
                suittemp = Suit.HEART;
                break;
            case 1:
                suittemp = Suit.DIAMOND;
                break;
            case 2:
                suittemp = Suit.CLUB;
                break;
            case 3:
                suittemp = Suit.SPADE;
                break;
            default:
                System.out.println("ERROR IN SUIT: " + suit + " is not between 0-3");

        }
        this.suit = suittemp;
        this.rank = rank;
        this.imgView = imgView;
    }
    public void changeImage(ImageView imgView) {
        this.imgView = imgView;
    }

    public void hide() {
        this.hidden = true;
        getChildren().addAll(cardBackImage());
    }

    public void show() {
        this.hidden = false;
        getChildren().addAll(this.imgView);
    }

    private ImageView cardBackImage() {
        ImageAdder ia = new ImageAdder();
        return ia.addImage(cardBackPath);
    }

    public Suit getSuit() {
        return this.suit;
    }

    public int getRank() {
        return this.rank;
    }

    public String toString() {
        return "[" + this.suit + " " + this.rank + "]";
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + Objects.hashCode(this.suit);
        hash = 31 * hash + this.rank;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Card other = (Card) obj;
        if (this.rank != other.rank) {
            return false;
        }
        if (this.suit != other.suit) {
            return false;
        }
        return true;
    }

}
