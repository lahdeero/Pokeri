package poker.logic;

import java.util.*;
import java.util.logging.Logger;

public class HandComparator {

    private final static Logger LOGGER = Logger.getLogger(GameLogic.class.getName());
    private List<Hand> hands;

    public boolean compareWithBoard(List<Player> players, List<Card> board) {
        this.hands = new ArrayList<Hand>();
        for (Player player : players) {
            int i = 0;
            Card[] cards = new Card[7];
            for (Card card : player.getCards()) {
                cards[i++] = card;
            }
            for (Card card : board) {
                cards[i++] = card;
            }
            Hand hand = new Hand(cards, player);
            hand.determineValue();
            hands.add(hand);
        }
        if (hands.isEmpty()) {
            LOGGER.severe("HANDCOMPARATOR: HAND EMPTY");
            return false;
        }
        Collections.sort(this.hands, Collections.reverseOrder());
        return true;
    }

    public List<Hand> getSortedHands() {
        return this.hands;
    }

    public List<Hand> getWinners() {
        if (this.hands.get(0).compareTo(hands.get(1)) == 0) {
            return hands;
        }
//        System.out.println(hands.get(0).compareTo(hands.get(1)));
//        System.out.println(hands.get(0).toStringDebug());
//        System.out.println(hands.get(1).toStringDebug());
        List<Hand> ret = new ArrayList<Hand>();
        ret.add(hands.get(0));
        return ret;
    }
}
