package poker.logic;

import java.nio.ByteBuffer;
import java.util.*;
import poker.logic.Card;
import javafx.scene.image.ImageView;
import poker.ui.ImageAdder.ImageAdder;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Stack;

public class Deck {

    private Stack<Card> cards;
    private String cardImagePath;
    private ImageAdder ia;
    private ImageView[] cardImages;

    /*
	 * CARD SHOULD RETURN IMAGE IT SHOULD NOT BE ADDED HERE
     */
    public Deck(String cardImagePath) {
        this.cardImagePath = cardImagePath;
        this.ia = new ImageAdder();
        this.cardImages = new ImageView[52];
    }

    public void refill() {
        for (int i = 1; i <= 13; i++) {
            cardImages[i - 1] = ia.addImage(cardImagePath + i + "_of_hearts.png");
        }
        for (int i = 1; i <= 13; i++) {
            cardImages[i + 12] = ia.addImage(cardImagePath + i + "_of_diamonds.png");
        }
        for (int i = 1; i <= 13; i++) {
            cardImages[i + 25] = ia.addImage(cardImagePath + i + "_of_clubs.png");
        }
        for (int i = 1; i <= 13; i++) {
            cardImages[i + 38] = ia.addImage(cardImagePath + i + "_of_spades.png");
        }
        cards = new Stack<>();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 13; j++) {
                Card card = new Card(i, j+1);
                card.changeImage(cardImages[i*13+j]);
                cards.push(card);
            }
        }
    }

    public Card drawCard() {
        return cards.pop();
    }

    public void shuffle() {
        byte[] bytes = ByteBuffer.allocate(Long.SIZE / Byte.SIZE).putLong(System.nanoTime()).array();
        Collections.shuffle(cards, new java.security.SecureRandom(bytes));
    }
}
