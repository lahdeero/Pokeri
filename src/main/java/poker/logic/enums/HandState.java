package poker.logic.enums;

public enum HandState {
    NEWHAND,PREFLOP,FLOP,TURN,RIVER,SHOWDOWN,ALLIN;
    private static HandState[] vals = values();

    public HandState next() {
        return vals[(this.ordinal()+1) % vals.length];
    }
    public int asNumber() {
        return this.ordinal();
    }
}

