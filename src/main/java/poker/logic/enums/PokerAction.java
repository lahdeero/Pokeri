package poker.logic.enums;
    public enum PokerAction {
        POST(0), FOLD(1), CHECK(2), CALL(3), BET(4), RAISE(5), ALLIN(6);

        private final int value;
        private PokerAction(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
