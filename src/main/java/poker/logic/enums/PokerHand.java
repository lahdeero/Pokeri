package poker.logic.enums;

public enum PokerHand {
    HIGHCARD(0), PAIR(1), TWOPAIRS(2), THREEOFKIND(3),STRAIGHT(4),FLUSH(5),FULLHOUSE(6),FOUROFKIND(7),STRAIGHTFLUSH(8);

    private final int value;
    private PokerHand(int value) { this.value = value; }

    public int getValue() { return value; }
}
