package poker.logic;

import poker.logic.enums.HandState;
import poker.logic.enums.PokerAction;

public class Action {

    private Player player;
    private PokerAction pokerAction;
    private double amount;
    private HandState handState;

    public Action(Player player, PokerAction action, double amount, HandState street) {
        this.player = player;
        this.pokerAction = action;
        this.amount = amount;
        this.handState = street;
    }

    public Player getPlayer() {
        return this.player;
    }

    public void setPokerAction(PokerAction pa) {
        this.pokerAction = pa;
    }

    public PokerAction getPokerAction() {
        return this.pokerAction;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return this.amount;
    }

    @Override
    public String toString() {
        return player.getName() + " " + pokerAction + " " + amount;
    }

    public HandState getHandState() {
        return this.handState;
    }
}
