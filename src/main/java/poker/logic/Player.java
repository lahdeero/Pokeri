package poker.logic;

import java.util.List;
import poker.logic.enums.HandState;

public interface Player {

    public void prepareForNewHand();

    public void prepareForAction(List<Card> board, double currentBet, double minRaise, HandState handState);

    public Action getAction();

    public double post(double amount);

    public void addChips(double amount);

    public double getChips();

    public double reduceChips(double amount);

    public String getName();

    public void addCard(Card card);

    public List<Card> getCards();

    public void clearCards();

    public void setAllin(boolean allin);

    public boolean getAllin();

    public void setPresent(boolean present);

    public boolean getPresent();

    public void setSitout(boolean sitout);

    public boolean getSitout();

    public boolean equals(Object o);

    public String getType();
}
