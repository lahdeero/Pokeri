package poker;

import javafx.application.Application;
import poker.ui.FxGui;

public class App {
    public static void main(String[] args) {
        Application.launch(FxGui.class, args);
    }
}

