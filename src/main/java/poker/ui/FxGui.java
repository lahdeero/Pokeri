package poker.ui;

import javafx.application.Application;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import java.util.logging.Logger;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Slider;
import poker.logic.Action;
import poker.logic.GameLogic;
import poker.logic.Player;
import poker.logic.bot.Human;
import poker.logic.bot.SimpleBot;
import poker.logic.Card;
import poker.logic.bot.RandomBot;
import poker.logic.enums.HandState;
import poker.logic.enums.PokerAction;

public class FxGui extends Application implements UserInterface {

    private final static Logger LOGGER = Logger.getLogger(FxGui.class.getName());
    private GameLogic logic;
    private Buttonizer buttonizer;
    private double resX = 1024;
    private double resY = 800;

    private final String cardImagePath = "/cards/png/";
    private HandUi humanPlayerUi, opponentUi, boardUi;
    private SimpleBooleanProperty playable;

    private HBox opponentCards = new HBox(20);
    private HBox humanPlayerCards = new HBox(20);
    private HBox boardCards = new HBox(20);

    private HBox playerButtons;
    private HBox endHandHBox;
    private StackPane bottomStackPane;

    private int dealerButton = 1;
    private int handNumber = 1;
    private StringProperty handNumberProperty = new SimpleStringProperty("HAND # 0");
    private StringProperty potSize = new SimpleStringProperty("POT: 0");
    private String chatStr = "CHAT";
    private StringProperty chat = new SimpleStringProperty(chatStr);

    private StringProperty sizingStr = new SimpleStringProperty("0");
    private double sizing = 0.0;

    private Player humanPlayer;
    private Player botPlayer;

    private StringProperty humanChipsStr = new SimpleStringProperty("100 $");
    private StringProperty botChipsStr = new SimpleStringProperty("100 $");

    private List<Player> uiPlayers;
    private Slider slider;

    public FxGui() {
        this.playable = new SimpleBooleanProperty(true);
        this.humanPlayer = new Human("HOOMAN", 500);
//        this.botPlayer = new SimpleBot("Simplebot", 100);
        this.botPlayer = new RandomBot("RandomBot", 500);
        uiPlayers = Arrays.asList(humanPlayer, botPlayer);
        this.logic = new GameLogic(uiPlayers, this, cardImagePath);
        this.slider = new Slider();
        this.buttonizer = new Buttonizer(resX, resY, logic, this);

        playerButtons = buttonizer.createButtonsHBox(true, logic.getSmallBlindSize());
    }

    @Override
    public void start(Stage primaryStage) {
        LOGGER.setLevel(Level.INFO);
        LOGGER.info("SHOULD BE CALLED JUST ONCE AT VERY START");

        primaryStage.setScene(new Scene(createContent(), resX, resY, Color.DARKSEAGREEN));
        primaryStage.setResizable(true);
        primaryStage.setTitle("PokeriBeli");
        primaryStage.show();
        logic.init();
        logic.startNewHand();
    }

    private Region createBackground() {
        Region background = new Region();
        background.setPrefSize(resX, resY);
        background.setStyle("-fx-background-color: rgba(0, 0, 0, 1)");
        return background;
    }

    private HBox createTopSideLayout() {
        HBox topSideLayout = new HBox(5);
        topSideLayout.setPadding(new Insets(5, 5, 5, 5));
        topSideLayout.setSpacing(10);
        return topSideLayout;
    }

    private Rectangle createTopBg() {
        Rectangle topBg = new Rectangle(resX * 0.8, resY * 0.85);
        topBg.setArcWidth(50);
        topBg.setArcHeight(50);
        topBg.setFill(Color.DARKGREEN);
        return topBg;
    }

    private Rectangle createChatBg() {
        Rectangle chatBg = new Rectangle(resX * 0.2, resY * 0.85);
        chatBg.setArcWidth(50);
        chatBg.setArcHeight(50);
        chatBg.setFill(Color.AQUA);
        return chatBg;
    }

    private VBox createChatVBox() {
        VBox chatVBox = new VBox(10);
        Label empty = new Label("");
        Label handNumberLabel = new Label();
        handNumberLabel.textProperty().bind(handNumberProperty);
        Label potSizeLabel = new Label();
        potSizeLabel.textProperty().bind(potSize);
        Label chatText = new Label();
        chatText.textProperty().bind(chat);
        chatVBox.getChildren().addAll(empty, handNumberLabel, potSizeLabel, chatText);
        return chatVBox;
    }

    private HBox createEndHandHBox() {
        HBox noHandHBox = new HBox(35);
        noHandHBox.setPadding(new Insets(0, 10, 0, resX / 8));
        noHandHBox.getChildren().add(buttonizer.createStartButton());
        return noHandHBox;
    }

    private VBox createBoardVBox() {
        VBox cardVBox = new VBox(80);
        cardVBox.setPadding(new Insets(30, 40, 30, 40));
        cardVBox.setAlignment(Pos.TOP_LEFT);
        cardVBox.getChildren().addAll(opponentCards, boardCards, humanPlayerCards);
        return cardVBox;
    }

    private Rectangle createBottomBG() {
        Rectangle bottomBG = new Rectangle(resX * 0.9, resY * 0.1);
        bottomBG.setArcWidth(50);
        bottomBG.setArcHeight(50);
        bottomBG.setFill(Color.ORANGE);
        return bottomBG;
    }

    private void createPlayersAndBoard() {
        humanPlayerUi = new HandUi(humanPlayerCards.getChildren());
        opponentUi = new HandUi(opponentCards.getChildren());
        boardUi = new HandUi(boardCards.getChildren());
        opponentCards.setPrefHeight(resY * 0);
        opponentCards.setAlignment(Pos.TOP_CENTER);
        boardCards.setPrefHeight(resY * 0.3);
        boardCards.setAlignment(Pos.CENTER_LEFT);
        humanPlayerCards.setPrefHeight(resY * 0.6);
        humanPlayerCards.setAlignment(Pos.BOTTOM_CENTER);
    }

    private VBox createBettingVBox() {
        Label botChips = new Label();
        Label potSizeLabel = new Label();
        Label humanChips = new Label();
        botChips.textProperty().bind(botChipsStr);
        botChips.setPrefHeight(resY * 0.1);
        potSizeLabel.textProperty().bind(potSize);
        potSizeLabel.setPrefHeight(resY * 0.55);
        humanChips.textProperty().bind(humanChipsStr);
        VBox bettingVBox = new VBox(10);
        bettingVBox.getChildren().addAll(botChips, potSizeLabel, humanChips);
        return bettingVBox;
    }

    private HBox createCardsAndChipsHBox() {
        HBox cardsAndBets = new HBox();
        VBox cardVBox = createBoardVBox();
        cardsAndBets.getChildren().add(cardVBox);
        cardsAndBets.getChildren().add(createBettingVBox());
        cardVBox.setPrefWidth(resX * 0.7);
        cardVBox.setPrefHeight(resY * 0.80);
        return cardsAndBets;
    }

    private MenuBar createMenuBar() {
        Menu fileMenu = new Menu("File");
        MenuItem exitItem = new MenuItem("Exit");
        fileMenu.getItems().add(exitItem);

        Menu gameMenu = new Menu("Game");
        MenuItem newGameItem = new MenuItem("New game");
        SeparatorMenuItem separator = new SeparatorMenuItem();
        MenuItem simpleBotItem = new MenuItem("SimpleBot");
        MenuItem randomBotItem = new MenuItem("RandomBot");
        MenuItem superBotItem = new MenuItem("SuperBot");
        
        gameMenu.getItems().add(newGameItem);
        gameMenu.getItems().add(separator);
        gameMenu.getItems().add(simpleBotItem);
        gameMenu.getItems().add(randomBotItem);
        gameMenu.getItems().add(superBotItem);

        Menu helpMenu = new Menu("Help");
        MenuItem aboutItem = new MenuItem("About");
        helpMenu.getItems().add(aboutItem);

        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().add(fileMenu);
        menuBar.getMenus().add(gameMenu);
        menuBar.getMenus().add(helpMenu);

        return menuBar;
    }

    private VBox createRootLayout() {
        Rectangle topBg = createTopBg();
        Rectangle chatBg = createChatBg();
        VBox chatVBox = createChatVBox();
        HBox cardsAndBets = createCardsAndChipsHBox();

        HBox topSideLayout = createTopSideLayout();

        topSideLayout.getChildren().addAll(new StackPane(topBg, cardsAndBets), new StackPane(chatBg, chatVBox));

        VBox rootLayout = new VBox(5);
        rootLayout.getChildren().add(createMenuBar());

        Rectangle bottomBg = createBottomBG();

        this.endHandHBox = createEndHandHBox();
        this.bottomStackPane = new StackPane(bottomBg, playerButtons);
        rootLayout.getChildren().addAll(topSideLayout, bottomStackPane);
        return rootLayout;
    }

    private Parent createContent() {
        Pane root = new Pane();
        root.setPrefSize(resX, resY);
        createPlayersAndBoard();

        Region background = createBackground();
        VBox rootLayout = createRootLayout();
        root.getChildren().addAll(background, rootLayout);

        return root;
    }

    public void dealCard(Player player, Card card) {
        if (player.getType().equals("human")) {
            card.show();
            humanPlayerUi.takeCard(card);
        } else {
            card.hide();
            opponentUi.takeCard(card);
        }
    }

    public void dealBoard(Card card) {
        card.show();
        boardUi.takeCard(card);
    }

    public void resetPot() {
        potSize.setValue("POT: " + 0.0);
    }

    public void updateUi(Action action, double pot, double amountForCall) {
        this.bottomStackPane.getChildren().remove(1);
        this.bottomStackPane.getChildren().add(buttonizer.createButtonsHBox(logic.isCurrentStreetBetted(), amountForCall));
        chatAppend(action.getPlayer().getName() + " " + action.getPokerAction().toString() + " " + action.getAmount());
        
        potSize.setValue("POT: " + pot + " $");
        setChipStrs(dealerButton);
    }

    private void setChipStrs(int button) {
        String buttonStr = "(B) ";
        if (button == 0) {
            humanChipsStr.setValue(buttonStr + "HUMAN: " + humanPlayer.getChips() + " $");
            botChipsStr.setValue("BOT: " + botPlayer.getChips() + " $");
        } else {
            humanChipsStr.setValue("HUMAN: " + humanPlayer.getChips() + " $");
            botChipsStr.setValue(buttonStr + "BOT: " + botPlayer.getChips() + " $");
        }
    }

    public void setPlayable(boolean value) {
        playable.set(value);
    }

    public void showDown() {
        for (Card card : botPlayer.getCards()) {
            card.show();
        }
    }

    public void clearBoard() {
        opponentUi.reset();
        boardUi.reset();
        humanPlayerUi.reset();
    }

    public void updateUiAfterHand(int button) {
        dealerButton = button;
        handNumberProperty.setValue("HAND # " + handNumber++);
        potSize.setValue("POT: " + 0.0 + " $");
        setChipStrs(button);
    }

    public void endHand() {
        this.bottomStackPane.getChildren().remove(1);
        this.bottomStackPane.getChildren().add(endHandHBox);
    }

    public void chatClear() {
        chatStr = "CHAT:";
        chat.setValue(chatStr);
    }

    public void chatAppend(String str) {
        chatStr = chatStr + "\n" + str;
        chat.setValue(chatStr);
    }

    public Player getHumanPlayer() {
        return this.humanPlayer;
    }

    public Player getBot() {
        return this.botPlayer;
    }

    public SimpleBooleanProperty getPlayable() {
        return this.playable;
    }
}
