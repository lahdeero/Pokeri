package poker.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;
import poker.logic.Action;
import poker.logic.GameLogic;
import poker.logic.enums.HandState;
import poker.logic.enums.PokerAction;

public class Buttonizer {

    private final static Logger LOGGER = Logger.getLogger(FxGui.class.getName());
    private GameLogic logic;
    private FxGui fxGui;
    private TextField textField;
    private Slider slider;
    private double resX;
    private double resY;

    public Buttonizer(double resX, double resY, GameLogic logic, FxGui fxGui) {
        this.resX = resX;
        this.resY = resY;
        this.logic = logic;
        this.fxGui = fxGui;
        this.slider = new Slider();
        this.textField = new TextField();
        initSlider();
    }

    public void initSlider() {
        slider.setMin(2);
        slider.setMax(fxGui.getHumanPlayer().getChips());
        slider.setShowTickLabels(true);
        textField.textProperty().bindBidirectional(slider.valueProperty(), new StringConverter<Number>() {
            @Override
            public String toString(Number t) {
                return Double.toString(Math.floor((double) t));
            }

            @Override
            public Number fromString(String str) {
                return str.isEmpty() ? Double.parseDouble("0.0") : Double.parseDouble(str);
            }
        });
    }

    public HBox createButtonsHBox(boolean bet, double amount) {
        HBox bHBox = new HBox(35);
        bHBox.setPadding(new Insets(0, 10, 0, resX / 8));
        List<Button> buttons = createButtons(bet, amount);
        bHBox.getChildren().addAll(buttons);
        bHBox.getChildren().add(textField);
        bHBox.getChildren().add(slider);
        bHBox.setAlignment(Pos.TOP_LEFT);
        return bHBox;
    }

    private List<Button> createButtons(boolean bet, double amount) {
        List<Button> buttons = new ArrayList<>();
        if (bet) {
            Button foldButton = new Button("FOLD");
            Button callButton = new Button("CALL");
            Button raiseButton = new Button("RAISE");
            buttons.addAll(Arrays.asList(foldButton, callButton, raiseButton));
        } else if (logic.getHandState() == HandState.PREFLOP) {
            buttons.add(new Button("FOLD"));
            buttons.add(new Button("CHECK"));
            buttons.add(new Button("RAISE"));
        } else {
            Button foldButton = new Button("FOLD");
            Button checkButton = new Button("CHECK");
            Button betButton = new Button("BET");
            buttons.addAll(Arrays.asList(foldButton, checkButton, betButton));
        }

        for (Button btn : buttons) {
            createListener(btn, PokerAction.valueOf(btn.getText()), amount);
            btn.disableProperty().bind(fxGui.getPlayable().not());
        }

        return buttons;
    }

    private void createListener(Button button, PokerAction pokerAction, double amount) {
        Action action = null;
        switch (pokerAction) {
            case FOLD:
                action = new Action(fxGui.getHumanPlayer(), pokerAction, 0, logic.getHandState());
                break;
            case CHECK:
                action = new Action(fxGui.getHumanPlayer(), pokerAction, 0, logic.getHandState());
                break;
            case CALL:
                action = new Action(fxGui.getHumanPlayer(), pokerAction, amount, logic.getHandState());
                break;
            case BET:
                action = new Action(fxGui.getHumanPlayer(), pokerAction, slider.valueProperty().doubleValue(), logic.getHandState());
                break;
            case RAISE:
                action = new Action(fxGui.getHumanPlayer(), pokerAction, slider.valueProperty().doubleValue(), logic.getHandState());
                break;
            case ALLIN:
                action = new Action(fxGui.getHumanPlayer(), pokerAction, fxGui.getHumanPlayer().getChips(), logic.getHandState());
                break;
            /*
			 * POST ISNT INCLUDED SO... there must be default */
            default:
                action = new Action(fxGui.getHumanPlayer(), pokerAction, fxGui.getHumanPlayer().getChips(), logic.getHandState());
                LOGGER.severe("ERROR: DEFAULT IN FXGUI LISTENER");
        }
        Action finalAction = action;
        button.setOnAction(event -> {
            if (finalAction.getPokerAction() == pokerAction.BET || finalAction.getPokerAction() == pokerAction.RAISE) {
                finalAction.setAmount((double) slider.valueProperty().intValue());
            }
            logic.pushHumanAction(finalAction);
        });
        updateSlider(amount);
    }

    public void updateSlider(double amountForCall) {
        slider.setMin(amountForCall * 2 < logic.getBigBlindSize() ? logic.getBigBlindSize() : amountForCall * 2);
        slider.setMax(fxGui.getHumanPlayer().getChips());
//        slider.setShowTickLabels(true);
//        textField.textProperty().bindBidirectional(slider.valueProperty(), new StringConverter<Number>() {
//            @Override
//            public String toString(Number t) {
//                return Double.toString(Math.floor((double) t));
//            }
//
//            @Override
//            public Number fromString(String str) {
//                return str.isEmpty() ? Double.parseDouble("0.0") : Double.parseDouble(str);
//            }
//        });
    }

    public Button createStartButton() {
        Button startButton = new Button("START");
        startButton.setOnAction(event -> {
            fxGui.clearBoard();
            fxGui.chatClear();
            logic.startNewHand();
        });
        startButton.disableProperty().bind(fxGui.getPlayable().not());
        return startButton;
    }

    public TextField getTextField() {
        return this.textField;
    }

    public Slider getSlider() {
        return this.slider;
    }

}
