package poker.ui;

import poker.logic.Card;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.List;

public class HandUi {

    private ObservableList<Node> nodeCards;
    private List<Card> orginalCards;
    private SimpleIntegerProperty value = new SimpleIntegerProperty(0);

    public HandUi(ObservableList<Node> cards) {
        this.nodeCards = cards;
        this.orginalCards = new ArrayList<>();
    }

    public void takeCard(Card card) {
        orginalCards.add(card);
        nodeCards.add(card);
    }

    public void reset() {
        nodeCards.clear();
        orginalCards.clear();
        value.set(0);
    }

    public List<Card> getCards() {
        return this.orginalCards;
    }

    public int size() {
        return nodeCards.size();
    }
}
