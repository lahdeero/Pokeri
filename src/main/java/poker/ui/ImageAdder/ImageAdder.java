package poker.ui.ImageAdder;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ImageAdder {

    public ImageView addImage(String path) {
        Image image = new Image(path) {};
        ImageView iv = new ImageView();
        iv.setImage(image);
        iv.setFitWidth(100);
        iv.setPreserveRatio(true);
        iv.setSmooth(true);
        iv.setCache(true);

        return iv;
    }
}
