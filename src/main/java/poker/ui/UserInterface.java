
package poker.ui;

import poker.logic.Action;
import poker.logic.Card;
import poker.logic.Player;


public interface UserInterface {

    public void setPlayable(boolean b);

    public void updateUi(Action sbPost, double pot, double amountForCall);

    public void dealCard(Player player, Card card);

    public void resetPot();

    public void dealBoard(Card card);

    public void chatAppend(String end_of_hand);

    public void showDown();

    public void updateUiAfterHand(int button);

    public void endHand();
    
}
